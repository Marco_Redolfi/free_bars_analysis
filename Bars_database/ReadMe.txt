MIGRATING ALTERNATE BARS DATABASE
*From Colombini, Seminara and Tubino, 1987 (kindly provided by prof. Marco Tubino).
*Arranged and integrated by prof. Walter Bertoldi
*Completed by Dr. Marco Redolfi
*The authors assume no responsibility or legal liability concerning the data’s accuracy, reliability and completeness

GENERAL NOTES
*units: cgs system
*Cr denotes the dimensionless Chézy coefficient
	+usually computed on the basis of velocity, slope, depth by considering the wetted perimeter (with some exceptions, e.g. Lanzoni et al., 2000)

DATA PROCESSING NOTES

Redolfi et al. (2020)
*uniform flow conditions are calculated under the wide-channel hypothesis, as suggested by the authors themselves
*the first experiment is not included, as it represents a transitional bed morphology

Garcia Lugo et al. (2015)
*experiments with W=20, and Q=2.0 and 2.5 l/s reclassified, from alternate bars to diagonal bars
	+after correspondence with Walter Bertoldi
	+after looking at wavelength, bed topography and pictures (wavelength <5 times the channel width, clear oblique fronts)
	+wavelength not included in the database, because its estimation is uncertain

Ahmari and da Silva (2011)
*experiments with faint alternate bars are not considered, due to the high risk of confusion with diagonal bars
*experiments showing alternate bars in transition to row-2 bars (bed form A-C2) are not included
