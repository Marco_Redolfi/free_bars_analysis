%% READING BARS DATABASE INTO A MATRIX WITH ALL NUMERICAL DATA
%Author: Marco Redolfi
%Created on: 16-Nov-2020

function [data_matr,dataset_name]=read_database_matrix()

%% Database file name

fname='Bars_database/Bars_database.dat'; 


%% Reading all lines

fid=fopen(fname,'r');
tline{1} = fgetl(fid);
while ischar(tline{end})
    tline{end+1} = fgetl(fid);
end
tline(end)=[];


%% Finding lines where new dataset start

expression='\(\d{4}\)'; %Expression defining author-year lines
author_ind=find(~cellfun(@isempty,regexp(tline,expression)));
Ndataset=length(author_ind);
author_ind(Ndataset+1)=length(tline)+1;


%% Building structure array

data_matr=[];
for i=1:Ndataset

	dataset_name{i}=tline{author_ind(i)}; %Dataset name
	data_fixed=str2num(tline{author_ind(i)+1}); %Dataset fixed data

	exp_count=0;
	for line_numb=author_ind(i)+2:author_ind(i+1)-1;
		data_line=str2num(tline{line_numb});
		if ~isempty(data_line)
			if length(data_line)~=8
				error(['Missing or exceeding data at line ',num2str(line_numb)])
			end
			exp_count=exp_count+1;
			data_matr(end+1,:)=[i,data_fixed(2:end),data_line];
		end
	end
	if exp_count~=data_fixed(1);
		error(['Missing/exceeding experiments in ',dataset_name{i}])
	end

	data_matr(data_matr(:)<0)=NaN; %Using NaN (instead of -1) to mark missing data

end
