%% READING BARS DATABASE INTO A STRUCTURE ARRAY
%Author: Marco Redolfi
%Created on: 16-Nov-2020

function db=read_database()

%% Database file name

fname='Bars_database/Bars_database.dat'; 


%% Reading all lines

fid=fopen(fname,'r');
tline{1} = fgetl(fid);
while ischar(tline{end})
    tline{end+1} = fgetl(fid);
end
tline(end)=[];


%% Finding lines where new dataset start

expression='\(\d{4}\)'; %Expression defining author-year lines
author_ind=find(~cellfun(@isempty,regexp(tline,expression)));
Ndataset=length(author_ind);
author_ind(Ndataset+1)=length(tline)+1;


%% Building structure array

db=struct; %Empty data structure 

for i=1:Ndataset

	db(i).name=tline{author_ind(i)}; %Dataset name
	db(i).data_fixed=str2num(tline{author_ind(i)+1}); %Dataset fixed data

	data_matr=[]; %Single matrix with all numeric data
	exp_count=0;
	for line_numb=author_ind(i)+2:author_ind(i+1)-1;
		data_line=str2num(tline{line_numb});
		if ~isempty(data_line)
			if length(data_line)~=8
				error(['Missing or exceeding data at line ',num2str(line_numb)])
			end
			exp_count=exp_count+1;
			data_matr(exp_count,:)=data_line;
		end
	end
	if exp_count~=db(i).data_fixed(1);
		error(['Missing/exceeding experiments in ',db(i).name])
	end

	data_matr(data_matr(:)<0)=NaN; %Using NaN (instead of -1) to mark missing data
	db(i).data=data_matr;

end
