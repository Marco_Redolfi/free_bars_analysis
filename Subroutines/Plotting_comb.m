%% Plotting

%1) Distribution of the relevant parameters

y_lim=[0 140];
Nbins=16;
figure
subplot(1,3,1)
	h1=histogram(beta,Nbins);
	grid on
	ylim(y_lim)
	xlabel('$$\beta\ [-]$$','Interpreter','latex')
	ylabel('Number of cases [-]','Interpreter','latex')
subplot(1,3,2)
	h2=histogram(theta0,Nbins,'FaceColor','g');
	grid on
	ylim(y_lim)
	xlabel('$$\theta_0\ [-]$$','Interpreter','latex')
subplot(1,3,3)
	h3=histogram(ds,Nbins,'FaceColor','r');
	grid on
	ylim(y_lim)
	xlabel('$$ds=d_{50}/D_0\ [-]$$','Interpreter','latex')

printpdf('Figures/Exp_parameters',[-0.6 0.2 0.8 -0.4],gcf,[18,10])


%2) Classification

%Position of the subplots
pos1=[0.1300 0.4096-0.05 0.4942+0.05 0.5154+0.05 ];
pos2=[0.1300 0.1100      0.4942+0.05 0.2157      ];
pos3=[0.6916 0.4096-0.05 0.2134      0.5154+0.05 ];

%Plotting limits
beta_scal_lim=[0.1,12];
theta_lim=[0,0.7];
Nbins_theta=18;
Nbins_beta=15;
Nlim_theta=[0 150];
Nlim_beta=[0 100];


for k=1:size(beta_C,2)

	figure('Name',fname{k})

		s1=subplot(3,3,[1,2,4,5],'Position',pos1);
		semilogy(theta0(indAB),1+eps(indAB,k),'or')
		hold on
		semilogy(theta0(~indAB),1+eps(~indAB,k),'ob')
		plot(theta_lim,[1,1],'--k','LineWidth',1.2)
		xlim(theta_lim)
		ylim(beta_scal_lim)
		grid on
		set(gca,'XTickLabel',[])
		ylabel('Scaled width-to-depth ratio $$\beta/\beta_C\ [-]$$','Interpreter','latex')
		leg={'Alternate bars','Other bedforms - plane bed'};
		legend(leg,'Interpreter','latex','Location','SE')
		set(gca,'YTickLabels',num2str(get(gca,'YTick')'))


	s2=subplot(3,3,[7,8],'Position',pos2);
		edges=linspace(theta_cr,theta_lim(2),Nbins_theta+1);
		h2=histogram(theta0,edges,'FaceColor','g');
		grid on
		xlim(theta_lim)
		ylim(Nlim_theta)
		xlabel('Shields number $$\theta_0\ [-]$$','Interpreter','latex')
		ylabel('\# of experiments','Interpreter','latex')
		set(gca,'FontSize',10)

	s3=subplot(3,3,[3,6],'Position',pos3);
		edges=logspace(log10(beta_scal_lim(1)),log10(beta_scal_lim(2)),Nbins_beta+1);
		h1=histogram(beta./beta_C(:,k),edges,'FaceColor','m','Orientation','horizontal');
		set(gca, 'yscale','log')
		grid on
		xlim(Nlim_beta)
		xlabel('\# of experiments','Interpreter','latex')
		set(gca,'YTickLabel',[])
		set(gca,'FontSize',10)


	printpdf(['Figures/',fname{k},'.pdf'],[0 0 0 0.4],gcf,[14,12])

end
