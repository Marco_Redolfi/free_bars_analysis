% ************************************************************************************
% EASY-TO-USE FUNCTION FOR COMPUTING CRITICAL BETA FOR BAR FORMATION
% Author: Marco Redolfi (marco.redolfi@unitn.it)
% NB: Not fully tested!
% ************************************************************************************


function [beta_C,wavenumber_C]=beta_crit_fct(theta0,ds0,varargin)

% OUTPUTS
% beta_C:       critical aspect ratio for bar formation [-]
% wavenumber_C: critical wavenumber [-]

% INPUTS
% theta0: Shields stress [-]
% ds0:    relative grain size [-]

% OPTIONAL PARAMETERS
% r:               Ikeda parameter for gravitational effect on bed load [-] (default=0.5)
% transport_f:     transport formula [string] (default='Parker')
%      current options: 'MP&M'; 'Wong&Parker'; 'Parker'; 'Engelund&Hansen'; 'Van_Rijn'
% resistance_f:    resistance formula [string] (default='Engelund&Hansen')
%      current options: 'Engelund&Hansen'; 'Engelund&Hansen_dunes'; 'Strickler'; 'Wilkerson&Parker'
% beta_init:       initial value of beta for the bisection method [-] (default=10)
% wavenumber_init: initial value of the wavenumber for the numerical solver [-] (default=0.5)
% beta_tol:        tolerance on the refinement of critical beta in the bisection method (default=1E-4)
% Nmax:            maximum number of iteration in the bisection method (default=100)
% c0:              user-provided value of dimensionless Chèzy coefficient
% cD:              user-provided value of the coefficient cD
% cT:              user-provided value of the coefficient cT
% Delta:           relative submerged density of sediment [-] (default=1.65)

% EXAMPLES OF USAGE
% beta_C=beta_crit_fct(theta0,ds0)
% [beta_C,wavenumber_C]=beta_crit_fct(theta0,ds0)
% beta_C=beta_crit_fct(theta0,ds0,'r',0.5)
% beta_C=beta_crit_fct(theta0,ds0,'Delta',1.65,'transport_f','MP&M')

%*********************************************************************

%Parsing optional inputs
p = inputParser;
addParameter(p,'r'              ,0.5              ,@isnumeric);
addParameter(p,'transport_f'    ,'Parker'         ,@ischar);
addParameter(p,'resistance_f'   ,'Engelund&Hansen',@ischar);
addParameter(p,'beta_init'      ,10               ,@isnumeric);
addParameter(p,'wavenumber_init',0.5              ,@isnumeric);
addParameter(p,'beta_tol'       ,1E-4             ,@isnumeric);
addParameter(p,'Nmax'           ,100              ,@isnumeric);
addParameter(p,'c0'             ,[]               ,@isnumeric);
addParameter(p,'cD'             ,[]               ,@isnumeric);
addParameter(p,'cT'             ,[]               ,@isnumeric);
addParameter(p,'Delta'          ,1.65             ,@isnumeric);
parse(p,varargin{:});

%Assinginig value to optional inputs
r              =p.Results.r;
transport_f    =p.Results.transport_f;
resistance_f   =p.Results.resistance_f;
wavenumber_init=p.Results.wavenumber_init;
beta_init      =p.Results.beta_init;
beta_tol       =p.Results.beta_tol;
Nmax           =p.Results.Nmax;
c0_user        =p.Results.c0;
cD_user        =p.Results.cD;
cT_user        =p.Results.cT;
Delta          =p.Results.Delta;


%% Definition of coefficients

[c0,cD,cT]=cderi(ds0,theta0,resistance_f,c0_user,cD_user,cT_user);
[PhiD,PhiT]=phideri(theta0,cD,transport_f);

if isnan(PhiT)
    warning('theta<=theta_cr')
    beta_C=NaN;
    wavenumber_C=NaN;
    return
end

%% Bisection method

%For each beta we can find the maximum growth rate
%   ->if positive: beta_C is lower than beta
%   ->if negative: beta_C is higher than beta

beta_inf=0;   %Lower limit of beta
beta_sup=Inf; %Upper limit of beta

beta=beta_init; %Initial value of beta for the bisection
beta_prev=beta; %Save current value of beta
step=0; %Step counter

while true
    step=step+1;

    %Growth rate as a function of wavenumber
    f=@(wavenumber) -real(omega_wavenumber(wavenumber,beta,theta0,ds0,r,Delta,c0,cD,cT,PhiD,PhiT));

    %Wavenumber corresponding to the maximum amplification
    wavenumber_max = fminsearch(f,wavenumber_init);

    %Maximum growth rate
    Omega_max=real(omega_wavenumber(wavenumber_max,beta,theta0,ds0,r,Delta,c0,cD,cT,PhiD,PhiT)); %Maximum amplification


    %Updating beta 
    if Omega_max>0 %If the maximum growth rate is positive beta_C is lower than the current beta 
        
        %Updating maximum value of beta
        beta_sup=beta; 
       
        %Value of beta to test in the following step
        beta=(beta_sup+beta_inf)/2; %Bisection of the inteval
   
    else  %If the maximum growth rate is negative beta_C is higher than the current beta 
        
        %Updating maximum value of beta
        beta_inf=beta;
       
        %Value of beta to test in the following step
        if isinf(beta_sup);
            beta=2*beta_inf;  %If current value of beta_sup is infinite: no bisection but twice beta_inf
        else    
            beta=(beta_inf+beta_sup)/2; %Bisection of the inteval
        end    
    end  

    if abs(beta-beta_prev)<beta_tol;
        break
    end    

    if step>=Nmax;
        error('Maximum number of iterations reached before obtaining beta critical within tolerance')
    end  

    wavenumber_init=wavenumber_max; %Initial wavenumber for the next iteration
    beta_prev=beta; %Storing previous value of beta

end

beta_C=beta;                 %Critical aspect ratio for bar formation
wavenumber_C=wavenumber_max; %Critical wavenumber
   
return


%% Function that gives growth rate as a function of the wavenumber

function omega=omega_wavenumber(wavenumber,beta,theta0,ds0,r,Delta,c0,cD,cT,PhiD,PhiT);

lambda=wavenumber*i; %Complex wavelength

%Definition of useful coefficients (see Camporeale et al., 2007)
f1=2/(1+2*cT);
f2=-2*cD/(1+2*cT);
P1=f1*PhiT;
P2=f2*PhiT+PhiD;
a6=r/(beta*sqrt(theta0));

Fr0=sqrt(ds0*Delta*theta0*c0^2); %Froude number

%The system of linear equations can be conveniently written as:
%M=[
%[ A1 , A5, A9       , 0               ] %Continuity
%[ A2 , 0 , A10      , A14             ] %X-Momentum
%[ 0  , A7, 0        , A15             ] %Y-Momentum
%[ A4 , A8, A12-omega, A16+Fr0^2*omega ] %Exner
%];
%where the Aj coefficients are given by the following matrix, with j indicating the linear index:
A=[
[ lambda              , -pi/2            , lambda              , 0               ] %Continuity
[ f1*beta/c0^2+lambda , 0                , (f2-1)*beta/c0^2    , lambda          ] %X-Momentum
[ 0                   , beta/c0^2+lambda , 0                   , pi/2            ] %Y-Momentum
[ P1*lambda           , -pi/2            , lambda*P2-pi^2*a6/4 , Fr0^2*pi^2*a6/4 ] %Exner (steady equation)
];

%The determinant of the matrix M is given by
%det(M)= (-A1*A7*A14 -A2*A5*A15 -A1*A7*A10*Fr0^2 +A2*A7*A9*Fr0^2)*omega +
% A1*A7*A12*A14 -A1*A7*A10*A16 +A1*A8*A10*A15 +A2*A5*A12*A15 +A2*A7*A9*A16 -A2*A8*A9*A15 -A4*A5*A10*A15 -A4*A7*A9*A14

%The condition det(M)=0 can be therefore expressed as: a*omega+b=0, where;
a=-A(1)*A(7)*A(14) -A(2)*A(5)*A(15) -A(1)*A(7)*A(10)*Fr0^2 +A(2)*A(7)*A(9)*Fr0^2;
b=+A(1)*A(7)*A(12)*A(14) -A(1)*A(7)*A(10)*A(16) +A(1)*A(8)*A(10)*A(15) +A(2)*A(5)*A(12)*A(15)...
  +A(2)*A(7)*A(9)*A(16) -A(2)*A(8)*A(9)*A(15) -A(4)*A(5)*A(10)*A(15) -A(4)*A(7)*A(9)*A(14);

omega=double(-b/a); %Complex frequency

return


%% Function that gives c0,cD,cT coefficients depending on the resistance formula
function [c0,cD,cT]=cderi(ds0,theta0,resistance_f,c0,cD,cT)

switch lower(resistance_f)
    
    case lower('Engelund&Hansen') %Engelund and Hansen (1967) formula (flat bed)

        if isempty(c0)
            c0=6+2.5*log(1/(2.5*ds0));
        end

        if isempty(cD); cD=2.5/c0; end
        if isempty(cT); cT=0;      end

    case lower('Engelund&Hansen_dunes') %Engelund and Hansen (1967) formula (dunes)

        f0=(0.06+0.4*theta0^2)/theta0; 
        
        if isempty(c0)
            c0=sqrt(f0)*(6+2.5*log(f0/(2.5*ds0)));
        end

        if isempty(cD); cD=2.5*sqrt(f0)/c0; end
        if isempty(cT)
            fT=theta0/f0*(-0.06/(theta0^2)+0.4); %Relative variation of f with theta
            cT=(1/2+2.5*sqrt(f0)/c0)*fT; 
        end

    case lower('Strickler') %Strickler (1923) formula

        if isempty(c0)
            c0=21.1/(9.81^0.5)/ds0^(1/6);
        end
        if isempty(cD); cD=1/6; end
        if isempty(cT); cT=0;   end

    case lower('Wilkerson&Parker') %Wilkerson and Parker (2011) formula

        if isempty(c0)
            c0=4.88/ds0^0.083;
        end
        if isempty(cD); cD=0.083; end
        if isempty(cT); cT=0;     end

    otherwise

        error('Error: Unknown resistance formula!')

end

return


%% Function that givers PhiD and PhiT coefficients depending on the transport formula

function [PhiD,PhiT,theta_cr]=phideri(theta0,cD,transport_f)

switch lower(transport_f) 

    case lower('MP&M') %Meyer-Peter and Muller (1948) formula
        
        theta_cr=0.047; %Critical Shield stress for incipient sediment motion

        if theta0<=theta_cr
            PhiT=NaN;
            PhiD=NaN;
            return
        end

        %Phi0=8*(theta0-theta_cr)^1.5;
        PhiT=1.5*theta0/(theta0-theta_cr);
        PhiD=0;

    case lower('Wong&Parker') %Wong and Parker (1996)

        theta_cr=0.047; %Critical Shield stress for incipient sediment motion

        if theta0<=theta_cr
            PhiT=NaN;
            PhiD=NaN;
            return
        end

        %Phi0=4.93.*(theta0-theta_cr)^1.6;
        PhiT=1.6*theta0/(theta0-theta_cr);
        PhiD=0;

    case lower('Parker_1978') %Parker (1978)

        theta_cr=0.03; %Critical Shield stress for incipient sediment motion

        if theta0<=theta_cr
            PhiT=NaN;
            PhiD=NaN;
            return
        end

        %Phi0=11.2*theta0^1.5*(1-theta_cr/theta0)^4.5;
        PhiT=1.5+4.5*theta_cr/(theta0-theta_cr);
        PhiD=0;

    case lower('Parker') %Parker (1990)

        theta_cr=0; %Critical Shield stress for incipient sediment motion

        if theta0<=theta_cr
            PhiT=NaN;
            PhiD=NaN;
            return
        end    

        %Parameters
        A=0.0386;
        B=0.853;
        C=5474;
        D=0.00218;

        x=theta0/A; %Normalized Shields stress in Parker formula
        if x>1.59
            Phi0=C*D*theta0^1.5*(1-B/x)^4.5;
            Phi_der=1.5*theta0^0.5*(1-B/x)^4.5*C*D+4.5*A*B*(1-B/x)^3.5*C*D/(theta0^0.5);
        elseif x>1
            Phi0=D*(theta0^1.5)*(exp(14.2*(x-1)-9.28*(x-1)^2));
            Phi_der=1/A*Phi0*(14.2-9.28*2*(x-1)) + 1.5*Phi0/theta0;
        else
            Phi0=D*theta0^1.5*x^14.2;
            Phi_der=14.2/A*D*theta0^1.5*x^13.2 + D*x^14.2*1.5*theta0^0.5;
        end

        PhiT=theta0/Phi0*Phi_der;
        PhiD=0;

    case lower('Engelund&Hansen') %Engelund and Hansen (1967)

        %alpha_EH=1;
        %Phi0=0.05*alpha_EH*c0^2*(theta0)^2.5;
        PhiT=2.5;
        PhiD=2*cD;    

    case lower('Van_Rijn') %Van Rijn (1984)  

        theta_cr=0.055; %Critical Shield stress for incipient sediment motion

        if theta0<=theta_cr
            PhiT=NaN;
            PhiD=NaN;
            return
        end
            
        if theta0/(theta0-theta_cr)<3.0
            PhiT=2.1*theta0/(theta0-theta_cr);
        else
            PhiT=1.5*theta0/(theta0-theta_cr);
        end    
        PhiD=0;

    otherwise

        error('Unknown transport formula!')

end

return
