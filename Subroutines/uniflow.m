%********************************************************************* 
% UNIFORM FLOW DISCHARGE IN A RECTANGULAR CHANNEL
% Author: Marco Redolfi
% ********************************************************************

function Q=uniflow(D,d50,W,slope,varargin)

% OUTPUT
% Q: Water discharge [m^3/s]

% INPUT
% D:     water depth [m]
% d50:   grain size [m]
% W:     channel width [m]
% slope: longitudinal gradient [m/m]

% OPTIONAL PARAMETERS
% g:            gravitational acceleration [m/s^2] (default=9.81 m/s^2)
% wide-channel: usage of wide channel hypothesis  [true/false] (default=true)
% resistance_f: resistance formula [string] (default='Engelund&Hansen')
% c:            user-defined value of the dimensionless Chézy coefficient [-]
% ks:           user-defined Gauckler-Strickler coefficient [m^(1/3)/s]
% Delta:        realtive sumberged density of sediment [-] (default=1.65)

% EXAMPLES
% uniflow(D,d50,W,slope)
% uniflow(D,d50,W,slope,'g',9.81)
% uniflow(D,d50,W,slope,'resistance_f','Engelund&Hansen','Wide_channel',false)

%*********************************************************************

%Parsing optional inputs
p = inputParser;
addParameter(p,'g'           ,9.81             ,@isnumeric);
addParameter(p,'wide_channel',true             ,@islogical);
addParameter(p,'resistance_f','Engelund&Hansen',@ischar);
addParameter(p,'c'           ,[]               ,@isnumeric);
addParameter(p,'ks'          ,[]               ,@isnumeric);
addParameter(p,'Delta'       ,1.65             ,@isnumeric);
parse(p,varargin{:});

%Assinginig value to optional inputs
g           =p.Results.g;
wide_channel=p.Results.wide_channel;
resistance_f=p.Results.resistance_f;
c           =p.Results.c;
ks          =p.Results.ks;
Delta       =p.Results.Delta;

	
if wide_channel    
    Rh=D;
else
    Rh=W*D/(W+2*D); %Hydraulic radius
end
ds=d50/Rh;


if ~isempty(c)
    warning('User provided value of d50 is ignored')
    
    if ~isempty(ks)
        warning('User provided ks is ignored')
    end

elseif ~isempty(ks)
    
    warning('User provided value of d50 is ignored')
    
    c=ks*Rh^(1/6)/sqrt(g);
        
else    
    
    switch lower(resistance_f)
        
        
        case lower('Engelund&Hansen')	%Engelund and Hansen (1967) formula (flat bed)

            c=6.+2.5*log(1./(2.5*ds));
    
        case lower('Engelund&Hansen_dunes') %%Engelund and Hansen (1967) formula (dunes)

            theta=slope/(Delta*ds);
            f=(0.06+0.04*theta^2)/theta;            
            c=f*(6.+2.5*log(f/(2.5*ds)));

        case lower('Strickler') %Strickler (1923) formula

            c=21.1/(9.81^0.5)/ds^(1./6.);
            
        otherwise
    
            error('Error: Unknown resistance formula!')
    end
end	

U = c*sqrt(g*slope*Rh); %Velocity 
Q = U*W*D; %Discharge
	
return
