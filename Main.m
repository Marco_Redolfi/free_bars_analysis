%% ANALYSIS OF FREE BARS DATASET
%Author: Marco Redolfi

close all
clear all
clc

addpath('Bars_database','Subroutines')


%% Analysis parameters
r=0.3;                     %Ikeda parameters for the effect of lateral slope
transport_f='Parker_1978'; %Sediment transport formula
theta_cr=0.03;             %Experiments with lower Shields number are excluded from the analysis
g=981;                     %Gravitational acceleration [cm/s^2]
exclude_list={};           %List of dataset to exclude from the analysis
exclude_armour=true;       %If true experiments with armoured bed are excluded from the analysis
exclude_lowtheta=true;     %If true experiments with theta<theta_cr are excluded
exclude_nodepth=false;     %If true experiments where no depth data are provided are excluded


%% Reading data from the free bars database

[data_matr,dataset_name]=read_database_matrix();
W    =data_matr(:,2);  %Channel width [cm]
Delta=data_matr(:,3);  %Relative submerged density of sediment [-]
d50  =data_matr(:,4);  %Representative grain size [cm]
Q    =data_matr(:,5);  %Water discharge [cm^3/s]
S0   =data_matr(:,6);  %Channel slope [-]
D0   =data_matr(:,7);  %Water depth [cm]
type =data_matr(:,12); %Type of observed bedforms (1->alternate bars)


%% Excluding undesired experiments

%1) Specific datasets
ID_excl=find(contains(dataset_name,exclude_list)); %ID of datasets to exclude from the analysis
ind_excl1=ismember(data_matr(:,1),ID_excl); %Indexes of experiments to keep

%2) Experiments with armoured bed
ind_excl2=(type==5) & exclude_armour;

%3) Experiments with low Shields number
theta0=S0.*D0./(Delta.*d50);
ind_excl3=(theta0<=theta_cr) & exclude_lowtheta;

%4) Experiments where no Depth/Chézy values were provided
ind_excl4=isnan(D0) & exclude_nodepth;

%Excluding experiments from the list of parameters
ind_excl=ind_excl1 | ind_excl2 | ind_excl3 | ind_excl4;
W    (ind_excl)=[];
Delta(ind_excl)=[];
d50  (ind_excl)=[];
Q    (ind_excl)=[];
S0   (ind_excl)=[];
D0   (ind_excl)=[];
type (ind_excl)=[];


%% Calculations

N=length(Q); % Number of datasets

%Calculation of uniform flow depth where not provided (if associated experiments are not excluded)
if ~exclude_nodepth
	for j=1:N
		if isnan(D0(j))

			%Calculation of depth (considering the hydraulic radius)
			f=@(x) uniflow(x,d50(j),W(j),S0(j),'wide_channel',true,'g',g)-Q(j);
			D0(j)=fsolve(f,10*d50(j));
			
		end
	end
end

%Relevant dimensionless parameters
beta=W./(2*D0); %Channel aspect ratio (i.e. half width-to-depth ratio)
theta0=S0.*D0./(Delta.*d50); %Shields number
ds=d50./D0; %Relative roughness

%Other parameters
U0=Q./(W.*D0); %Average velocity [cm/s]
Rh0=(W.*D0)./(W+2*D0); %Hydraulic radius [cm]
Fr=U0./sqrt(g*D0); %Froude number


%% Critical aspect ratio according to theoretical criteria

% 1) Complete model by Colombini et al. (1987)
for j=1:N

	if isnan(ds(j))
		beta_C(j,1)=NaN;
	else	
		beta_C(j,1)=beta_crit_fct(theta0(j),ds(j),'Delta',Delta(j),'r',r,'transport_f',transport_f);
	end
end
fname{1}='Colombini_et_al_1987'; 

% 2) Simplified model by Redolfi (2021)
for j=1:N

	if isnan(ds(j))
		beta_C(j,2)=NaN;
	else	
		beta_C(j,2)=beta_crit_fct_simpl(theta0(j),ds(j),'Delta',Delta(j),'r',r,'transport_f',transport_f);
	end
end
fname{2}='Redolfi_2021';


%% Calculating relevant metrics

%Index of experiments where alternate bars are observed
indAB=(type==1);

for k=1:size(beta_C,2)

	eps(:,k)=(beta-beta_C(:,k))./beta_C(:,k);
	xi(:,k)=log(1+eps(:,k));

	%Confusion matrix
	C{k}=confusionmat(indAB,eps(:,k)>0);

	%Binary classification metrics
	ACC(k)=(C{k}(1,1)+C{k}(2,2))/sum(C{k}(:)) *100; %Accuracy 
	TPR(k)=C{k}(2,2)/sum(C{k}(2,:)) *100; %Sensitivity
	TNR(k)=C{k}(1,1)/sum(C{k}(1,:)) *100; %Specificity
	BA(k)=(TPR(k)+TNR(k))/2; %Balanced accuracy

	%Deviation-based error index (ratio between the sum of |xi| for incorrect and total cases)
	ind_fail(:,k)=(eps(:,k)>0)~=indAB;         
	dev(k)=nansum(abs(xi( ind_fail(:,k),k)))./nansum(abs( xi(:,k)))*100;
end


%% Plotting

Plotting_comb
